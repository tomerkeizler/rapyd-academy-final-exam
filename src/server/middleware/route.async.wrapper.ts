import { Request, Response, NextFunction, RequestHandler } from 'express'

// Async Middleware Function
type AsyncMidFunc = (req: Request,res: Response,next: NextFunction) => Promise<void>;

// export default (fn: AsyncMidFunc) : RequestHandler => (req, res, next) => {
  export default (fn: AsyncMidFunc) => (req: Request, res: Response, next: NextFunction) => {
    fn(req, res, next).catch(next);
    // .catch((err)=> next(err))
  };
  