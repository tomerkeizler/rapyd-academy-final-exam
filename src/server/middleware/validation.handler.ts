import { Request, Response, NextFunction } from "express";
import Ajv, { JTDSchemaType } from "ajv/dist/jtd";

const ajv = new Ajv();

function validationHandler<T>(schema: JTDSchemaType<T>) {
  return (req: Request, res: Response, next: NextFunction) => {

    // validateObj is a type guard for objects - type is inferred from schema type
    const validateObj = ajv.compile(schema);
    const valid = validateObj(req.body);

    if (!valid && validateObj.errors) {
      const { instancePath, message } = validateObj.errors[0]
      res.status(422).json({ error: `${instancePath.slice(1)} ${message}` });
      // throw { message: `${instancePath.slice(1)} ${message}` };
    }
    else next();
  };
};

export default validationHandler;
