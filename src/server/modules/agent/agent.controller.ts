import raw from "../../middleware/route.async.wrapper";
import express, { Request, Response } from "express";
import * as agentService from "./agent.service";
import { Agent, agentSchema } from "./agent.type.validate";
import validationHandler from "../../middleware/validation.handler"

const router = express.Router();

// parse json req.body on post routes
router.use(express.json());

// CREATES A NEW AGENT
router.post(
  "/",
  validationHandler<Agent>(agentSchema),
  raw(async (req: Request, res: Response) => {
    const newAgent: Agent = req.body as Agent;
    console.log("create a agent:", newAgent);

    const result = await agentService.createAgent(newAgent);
    if (result.affectedRows) {
      res.status(200).json(`Agent was added successfully`);
    } else {
      res.status(404).json({ message: `Error while adding agent` });
    }
  })
);

// GETS ALL AGENTS
router.get(
  "/",
  raw(async (req: Request, res: Response) => {
    const agentPlaces = await agentService.getAllAgents();
    res.status(200).json(agentPlaces);
  })
);

// GETS A SINGLE AGENT
router.get(
  "/:id",
  raw(async (req: Request, res: Response) => {
    const agentID = parseInt(req.params.id);

    const agentPlaces = await agentService.getSingleAgent(agentID);
    if (agentPlaces.length > 0) {
      res.status(200).json(agentPlaces);
    } else {
      res.status(404).json({ message: `Agent ${agentID} was not found` });
    }
  })
);

export default router;
