import { connection as db } from "../../db/mysql.connection";
import { OkPacket, RowDataPacket } from "mysql2/promise";
import { Agent } from "./agent.type.validate";

export const createAgent = async (newAgent: Agent): Promise<OkPacket> => {
  const sql = `INSERT INTO agent SET ?`;
  const [result] = await db.query(sql, newAgent);
  return result as OkPacket;
};

export const getAllAgents = async (): Promise<any[]> => {
  const sql = `
  SELECT agent.*, place.name AS place_name, place.hits AS place_hits
  FROM agent
  JOIN place ON agent.id = place.agent_id
  `;
  const [result] = await db.query(sql);
  
  const rows: RowDataPacket[] = result as RowDataPacket[];
  // const agents: Agent[] = rows as Agent[];
  return rows;
};

export const getSingleAgent = async (agentID: number): Promise<any[]> => {
  const sql = `
  SELECT agent.*, place.name AS place_name, place.hits AS place_hits
  FROM agent
  JOIN place ON agent.id = place.agent_id
  WHERE agent.id = ?
  `;
  const [result] = await db.query(sql, agentID);

  const rows: RowDataPacket[] = result as RowDataPacket[];
  return rows;
}
