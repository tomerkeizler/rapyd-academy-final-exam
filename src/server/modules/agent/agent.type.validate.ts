import { JTDSchemaType } from "ajv/dist/jtd";

// -------------------------

enum TYPE {
  Human = "HUMAN",
  Android = "ANDROID",
  Sensor = "SENSOR",
  Machine = "MACHINE"
}

export interface Agent {
  // id?: number;
  name: string;
  type: string;
  // type: TYPE;
  owner?: string;
  status: string;
  // status: "pending" | "active" | "retired";
}

// -------------------------

export const agentSchema: JTDSchemaType<Agent> = {
  optionalProperties: {
    // id: { type: "int32" },
    owner: { type: "string" },
  },
  properties: {
    name: { type: "string" },
    type: { type: "string" },
    status: { type: "string" },
  },
};
