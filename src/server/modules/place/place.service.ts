import { connection as db } from "../../db/mysql.connection";
import { OkPacket, RowDataPacket } from "mysql2/promise";
import { Place } from "./place.type.validate";

export const createPlace = async (newPlace: Place): Promise<OkPacket> => {
  const sql = `INSERT INTO place SET ?`;
  const [result] = await db.query(sql, newPlace);
  return result as OkPacket;
};

export const getAllPlaces = async (): Promise<Place[]> => {
  const sql = `SELECT * FROM place`;
  const [result] = await db.query(sql);
  
  const rows: RowDataPacket[] = result as RowDataPacket[];
  const places: Place[] = rows as Place[];
  return places;
};
