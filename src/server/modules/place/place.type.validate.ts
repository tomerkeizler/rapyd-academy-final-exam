import { JTDSchemaType } from "ajv/dist/jtd";

// -------------------------

export interface Place {
  // id?: number;
  agent_id: number;
  name: string;
  hits: number;
}

// -------------------------

export const placeSchema: JTDSchemaType<Place> = {
  // optionalProperties: {
  //   id: { type: "int32" },
  // },
  properties: {
    agent_id: { type: "int32" },
    name: { type: "string" },
    hits: { type: "int32" },
  },
};
