  import raw from "../../middleware/route.async.wrapper";
  import express, { Request, Response } from "express";
  import * as placeService from "./place.service";
  import { Place, placeSchema } from "./place.type.validate";
  import validationHandler from "../../middleware/validation.handler"
  
  const router = express.Router();
  
  // parse json req.body on post routes
  router.use(express.json());
  
  // CREATES A NEW PLACE
  router.post(
    "/",
    validationHandler<Place>(placeSchema),
    raw(async (req: Request, res: Response) => {
      const newPlace: Place = req.body as Place;
      console.log("create a place:", newPlace);
  
      const result = await placeService.createPlace(newPlace);
      if (result.affectedRows) {
        res.status(200).json(`place was added successfully`);
      } else {
        res.status(404).json({ message: `Error while adding place` });
      }
    })
  );

  // GETS ALL PLACES
router.get(
  "/",
  raw(async (req: Request, res: Response) => {
    const places: Place[] = await placeService.getAllPlaces();
    res.status(200).json(places);
  })
);

  export default router;
  