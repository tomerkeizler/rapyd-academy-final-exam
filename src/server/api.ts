import express from 'express'
import morgan from 'morgan'
import cors from 'cors'

import agent_controller from './modules/agent/agent.controller';
import place_controller from './modules/place/place.controller';
import {error_handler,error_handler2,not_found} from './middleware/errors.handler';   
import env from "./utils/util.env"
import { connect } from "./db/mysql.connection"

const PORT = Number(env('PORT'))
const HOST = env('HOST')

const app = express();

// middleware
app.use(cors());
app.use(morgan('dev'))

// routing
app.use('/api/agents', agent_controller);
app.use('/api/places', place_controller);

// central error handling
app.use(error_handler);
app.use(error_handler2);

//when no routes were matched...
app.use('*', not_found)

//start the express api server
;(async ()=> {
  await connect() //connect to mySQL
  await app.listen(PORT,HOST);
  console.log(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
})().catch(console.log)